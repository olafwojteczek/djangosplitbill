from django.urls import path
from . import views


urlpatterns = [
    path('list_bills/', views.list_bills, name="view_bills"),
    path('add_bill/', views.add_bill, name="add_bill"),
    path('', views.home, name="home")
]