import datetime

from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User
from django.utils.timezone import now


class Bill(models.Model):
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateField(default= now)
    name = models.CharField(max_length=100)
    paid_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class BillForm(ModelForm):
    class Meta:
        model = Bill
        fields = ['amount', 'date', 'name', 'paid_by']