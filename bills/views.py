from django.shortcuts import render
from .models import Bill, BillForm
# from .forms import BillForm

def home(request):
    return render(request, 'home.html')

def list_bills(request):
    bills = Bill.objects.all()
    return render(request, 'list_bills.html', {'bills': bills})

def add_bill(request):
    if request.method == 'POST':
        form = BillForm(request.POST)
        if form.is_valid():
            form.save()
            # Add any additional logic if needed
    else:
        form = BillForm()

    return render(request, 'add_bill.html', {'formset': form})
