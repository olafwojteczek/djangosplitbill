# Generated by Django 5.0.1 on 2024-01-17 10:22

import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bills', '0002_bill_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bill',
            name='date',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
